import React from "react";

//effect of different data inside tags
//string== works normally inside tag
//number= works normally inside tag
//boolean => boolean are not shown in tag
//array
// => wrapper are disappear
// => element are placed one by one without comma
//null ===> it will not shown
//undefined ===> it will not shown
//obj ==> you can not use obj as react child or html element child

const PlayWithData = () => {
  //   let a = { name: "nitan", age: 29 };
  let a = 3;

  return <div>{a}</div>;
};

export default PlayWithData;
