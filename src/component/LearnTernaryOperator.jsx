import React from "react";

const LearnTernaryOperator = () => {
  let age = 15;
  let isMarried = true;
  let num = 18;
  // num ===16 he is 16
  // num ===17 he is 17
  // num=== 18 he is 18
  // else he is not in list

  // marks
  //   below and equal 39    fail
  //   [40-49]   third division
  //   [50 -59] second division
  //     [60-79]  first division
  //     80 to above distinction

  //we must have else part in ternary operator

  return (
    <div>
      {age > 17 ? "he can enter room" : "he can not enter room"}

      {isMarried ? "he is Married" : "he is not married"}

      {num === 16
        ? "he is 16"
        : num === 17
        ? "he is 17"
        : num === 18
        ? "he is 18"
        : "he is not in list"}
    </div>
  );
};

export default LearnTernaryOperator;
