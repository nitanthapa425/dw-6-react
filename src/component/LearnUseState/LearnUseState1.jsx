import React, { useState } from "react";



const LearnUseState1 = () => {
  let [name, setName] = useState("nitan"); //hari

  console.log("the component is execute");

  return (
    <div>
      {name}
      <br></br>
      <button
        onClick={() => {
          setName("hari");
        }}
      >
        Change name
      </button>
    </div>
  );
};

export default LearnUseState1;
