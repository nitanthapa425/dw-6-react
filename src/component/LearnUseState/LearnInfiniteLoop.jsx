import React, { useState } from "react";

const LearnInfiniteLoop = () => {
  let [count, setCount] = useState(0);

  // always put setCount on an event (eg button click) or inside useEffect
  // other wise it will trigger infinite loop
  setCount(count + 1);

  return (
    <div>
      count is {count}
      <br></br>
      <button
        onClick={() => {
          setCount(count + 1);
        }}
      >
        Increment
      </button>
    </div>
  );
};

export default LearnInfiniteLoop;
