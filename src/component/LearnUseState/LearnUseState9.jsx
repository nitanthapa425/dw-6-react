import React, { useState } from "react";

const LearnUseState9 = () => {
  let [count1, setCount1] = useState(0); //2
  let [count2, setCount2] = useState(100); //102
  console.log("hello");
  return (
    <div>
      count1 = {count1}
      <br></br>
      count2 = {count2}
      <br></br>
      <button
        onClick={() => {
          setCount1(count1 + 1); //2
        }}
      >
        Increment count1
      </button>
      <br></br>
      <button
        onClick={() => {
          setCount2(count2 + 1); //101
        }}
      >
        Increment count2
      </button>
    </div>
  );
};

export default LearnUseState9;
