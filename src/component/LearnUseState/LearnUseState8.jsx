import React, { useState } from "react";

const LearnUseState8 = () => {
  // a page will render only if state variable change
  let [count1, setCount1] = useState(0); //1

  console.log("Hello");

  return (
    <div>
      count1 is {count1}
      <button
        onClick={() => {
          setCount1(0);
        }}
      >
        Reset to 0
      </button>
      <button
        onClick={() => {
          setCount1(count1 + 1);
        }}
      >
        Increment count1
      </button>
    </div>
  );
};

export default LearnUseState8;
