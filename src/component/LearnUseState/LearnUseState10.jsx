import React, { useState } from "react";

//for primitive
// a component will render if value is changed

//for not primitive
// a component will render if memory is changed
const LearnUseState10 = () => {
  let [count, setCount] = useState([1, 2]);
  console.log("hello");
  return (
    <div>
      count = {count}
      <br></br>
      <button
        onClick={() => {
          setCount([1, 2]);
        }}
      >
        increment count1
      </button>
    </div>
  );
};

export default LearnUseState10;
