import React, { useState } from "react";

const LearnUseState7 = () => {
  let [count1, setCount1] = useState(0); //1

  console.log("Hello");

  return (
    <div>
      count1 is {count1}
      <br></br>
      <button
        onClick={() => {
          setCount1(count1 + 1);
        }}
      >
        Increment count1
      </button>
    </div>
  );
};

export default LearnUseState7;
