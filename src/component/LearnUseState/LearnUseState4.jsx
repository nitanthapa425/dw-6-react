import React, { useState } from "react";

const LearnUseState4 = () => {
  let [show, setShow] = useState(true); //false

  //if show true  button name hide
  // if show false button name show
  return (
    <div>
      {show ? <img src="./favicon.ico" alt="favicon"></img> : null}

      <br></br>
      <button
        onClick={() => {
          setShow(!show);
        }}
      >
        {show ? "Hide" : "Show"}
      </button>
    </div>
  );
};

export default LearnUseState4;
