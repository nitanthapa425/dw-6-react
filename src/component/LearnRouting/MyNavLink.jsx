import React from "react";
import { NavLink } from "react-router-dom";

const MyNavLink = () => {
  return (
    <div>
      {/* change url on click of link */}
      <NavLink to="/about" style={{ marginRight: "100px" }}>
        About
      </NavLink>
      <NavLink to="/" style={{ marginRight: "100px" }}>
        Home
      </NavLink>
      <NavLink to="/contacts" style={{ marginRight: "100px" }}>
        Read all contacts
      </NavLink>
      <NavLink to="/contacts/create" style={{ marginRight: "100px" }}>
        create contact
      </NavLink>
    </div>
  );
};

export default MyNavLink;
