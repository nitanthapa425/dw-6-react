import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

const ReadContacts = () => {
  let navigate = useNavigate();
  let [contacts, setContacts] = useState([]);

  let getReadContacts = async () => {
    let response = await axios({
      url: "http://localhost:8000/api/v1/contacts",
      method: "GET",
    });

    setContacts(response.data.data.results);
  };

  useEffect(() => {
    getReadContacts();
  }, []);

  return (
    <div>
      {contacts.map((value, i) => {
        return (
          <div
            key={i}
            style={{ border: "solid red 2px", marginBottom: "15px" }}
          >
            <p>Full name:{value.fullName}</p>
            <p>Email:{value.email}</p>
            <p>Phone Number:{value.phoneNumber}</p>
            <p>Created At:{new Date(value.createdAt).toLocaleString()}</p>
            <button
              onClick={async () => {
                await axios({
                  url: `http://localhost:8000/api/v1/contacts/${value._id}`,
                  method: "DELETE",
                });
                getReadContacts();
                // to update data as in the database after delete
                // invalidate --> hitting api inside an api hit
              }}
            >
              Delete
            </button>

            {/* //localhost:3000/contact/1234214124 */}
            <button
              onClick={() => {
                navigate(`/contacts/${value._id}`);
              }}
            >
              View
            </button>

            <button
              onClick={() => {
                navigate(`/contacts/update/${value._id}`);
              }}
            >
              Edit
            </button>
          </div>
        );
      })}
    </div>
    // must have key={i} to give unique value in div
  );
};

export default ReadContacts;
