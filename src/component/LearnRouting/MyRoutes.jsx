import React from "react";
import { Outlet, Route, Routes } from "react-router-dom";
import Home from "./Home";
import About from "./About";
import Contact from "./Contact";
import ContactDetail from "./ContactDetail";
import ContactDetails from "./ContactDetails";
import CreateContacts from "./CreateContacts";
import UpdateContacts from "./UpdateContacts";
import ReadContacts from "./ReadContacts";
import LearnForm6 from "../LearnForm/LearnForm6";

const MyRoutes = () => {
  return (
    <div>
      {/*  show component for different url */}

      {/* if conflict occurred while choosing component
      first more specific route component  is taken
       */}

      <Routes>
        <Route path="/" element={<Home></Home>}></Route>
        <Route path="/about" element={<About></About>}></Route>
        <Route path="/contact" element={<Contact></Contact>}></Route>
        <Route
          path="/contact/:id1/ram/:id2"
          element={<ContactDetail></ContactDetail>}
        ></Route>
        <Route path="/contact/a" element={<div>this is contact a</div>}></Route>
        {/* localhost:3000/contact/any/any/nitan*/}
        <Route
          path="/contact/:a/:c/nitan"
          element={<div>this is contact **************</div>}
        ></Route>
        <Route path="*" element={<div>404 page</div>}></Route>
        {/* <Route path="/blogs" element={<div>my blogs</div>}></Route>
        <Route path="/blogs/:id" element={<div>my blogs id</div>}></Route>
        <Route path="/blogs/create" element={<div>my blogs create</div>}></Route>
        <Route path="/blogs/update/:id" element={<div>my blogs update</div>}></Route> */}

        <Route
          path="blogs"
          element={
            <div>
              <Outlet></Outlet>I am parent
            </div>
          }
        >
          <Route index element={<div>Read all Blog</div>}></Route>
          <Route path=":id" element={<div>Detail page</div>}></Route>
          <Route path="create" element={<div>Create blog</div>}></Route>
          <Route
            path="update"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route path=":id" element={<div>update page</div>}></Route>
          </Route>
        </Route>

        <Route
          path="contacts"
          element={
            <div>
              <Outlet></Outlet>
            </div>
          }
        >
          <Route
            index
            element={
              <div>
                <ReadContacts></ReadContacts>
              </div>
            }
          ></Route>
          <Route
            path=":id"
            element={
              <div>
                <ContactDetails></ContactDetails>
              </div>
            }
          ></Route>
          <Route
            path="create"
            element={
              <div>
                <LearnForm6></LearnForm6>
              </div>
            }
          ></Route>
          <Route
            path="update"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route
              path=":id"
              element={
                <div>
                  <UpdateContacts></UpdateContacts>
                </div>
              }
            ></Route>
          </Route>
        </Route>
      </Routes>
    </div>
  );
};

export default MyRoutes;
