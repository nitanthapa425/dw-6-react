import React from "react";
import MyNavLink from "./MyNavLink";
import MyRoutes from "./MyRoutes";

//dynamic routing

const Routing1 = () => {
  return (
    <div>
      <MyNavLink></MyNavLink>
      <MyRoutes></MyRoutes>
    </div>
  );
};

export default Routing1;
