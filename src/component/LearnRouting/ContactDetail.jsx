import React from "react";
import { useParams } from "react-router-dom";

//get dynamic routes

// /contacts/:id1/ram/:id2
const ContactDetail = () => {
  let params = useParams();

  console.log(params);
  console.log(params.id1);
  console.log(params.id2);

  return <div>ContactDetail</div>;
};

export default ContactDetail;
