import React from "react";
import { useSearchParams } from "react-router-dom";

const Contact = () => {
  // let [name, setName] = useState()
  // localhost:3000/contact?name=nitan
  let [queryParameter] = useSearchParams();

  console.log(queryParameter.get("name"));
  console.log(queryParameter.get("age"));
  return <div>Contact</div>;
};

export default Contact;
