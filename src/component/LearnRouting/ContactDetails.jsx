import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

const ContactDetails = () => {
  let params = useParams();
  let [detail, setDetail] = useState({});

  let id = params.id;

  let getDetails = async () => {
    let response = await axios({
      url: `http://localhost:8000/api/v1/contacts/${id}`,
      method: "GET",
    });

    setDetail(response.data.data);
  };

  useEffect(() => {
    getDetails();
  }, []);

  console.log(detail);

  return (
    <div>
      <p>Address:{detail.address}</p>
      <p>email:{detail.email}</p>
      <p>phoneNumber:{detail.phoneNumber}</p>
    </div>
  );
};

export default ContactDetails;
