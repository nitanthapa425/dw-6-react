let Abc = (props) => {
  return (
    <div>
      <p>name is {props.name}</p>
      <p>address is {props.address}</p>
      <p>age is {props.age}</p>
    </div>
  );
};

export default Abc;
