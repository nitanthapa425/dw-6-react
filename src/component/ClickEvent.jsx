import React from "react";

const ClickEvent = () => {
  return (
    <div>
      ClickEvent
      <br></br>
      <p
        onClick={() => {
          console.log("this is paragraph");
        }}
      >
        this is para
      </p>
      <button
        onClick={() => {
          console.log("my name is nitan thapa");
        }}
      >
        Click me
      </button>
    </div>
  );
};

export default ClickEvent;
