import React, { useState } from "react";

//name
//age
//address
//password
//description

const LearnForm2 = () => {
  let [name, setName] = useState(""); //"a"

  let [age, setAge] = useState();

  let [address, setAddress] = useState();
  let [password, setPassword] = useState();
  let [description, setDescription] = useState();

  return (
    <div>
      <form>
        <div>
          <label htmlFor="name">Name: </label>
          <input
            id="name"
            type="text"
            placeholder="enter your name"
            value={name} //"a"
            onChange={(e) => {
              setName(e.target.value);
            }}
          ></input>
        </div>

        <div>
          <label htmlFor="age">Age</label>
          <input
            id="age"
            type="number"
            value={age}
            onChange={(e) => {
              setAge(e.target.value);
            }}
          ></input>
        </div>
        <div>
          <label htmlFor="address">Address</label>
          <input
            id="address"
            type="text"
            value={address}
            onChange={(e) => {
              setAddress(e.target.value);
            }}
          ></input>
        </div>
        <div>
          <label htmlFor="password">Password</label>
          <input
            id="password"
            type="password"
            value={password}
            onChange={(e) => {
              setPassword(e.target.value);
            }}
          ></input>
        </div>
        <div>
          <label htmlFor="description">Description</label>
          <textarea
            id="description"
            value={description}
            onChange={(e) => {
              setDescription(e.target.value);
            }}
          ></textarea>
        </div>
      </form>
    </div>
  );
};

export default LearnForm2;
