import React, { useState } from "react";
// gender => male, female, other
const LearnForm4 = () => {
  let [gender, setGender] = useState("male");
  return (
    <div>
      <label htmlFor="male">Male</label>
      <input
        id="male"
        type="radio"
        value="male"
        checked={gender === "male"}
        onChange={(e) => {
          setGender(e.target.value);
        }}
      ></input>

      <label htmlFor="female">Female</label>
      <input
        id="female"
        type="radio"
        value="female"
        checked={gender === "female"}
        onChange={(e) => {
          setGender(e.target.value);
        }}
      ></input>

      <label htmlFor="other">Other</label>
      <input
        id="other"
        type="radio"
        value="other"
        checked={gender === "other"}
        onChange={(e) => {
          setGender(e.target.value);
        }}
      ></input>
    </div>
  );
};

export default LearnForm4;
