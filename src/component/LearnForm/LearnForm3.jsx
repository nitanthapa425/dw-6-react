import React, { useState } from "react";

const LearnForm3 = () => {
  let [country, setCountry] = useState("nepal");

  //   to select
  // option value and state value must be same
  return (
    <div>
      <form>
        <label>Country: </label>
        <select
          value={country} //chi
          onChange={(e) => {
            setCountry(e.target.value);
          }}
        >
          <option value="nepal">Nepal</option>
          <option value="india">India</option>
          <option value="china">China</option>
          <option value="pakistan">Pakistan</option>
        </select>
      </form>
    </div>
  );
};

export default LearnForm3;
