import React, { useState } from "react";

//checkbox
//isMarried

const LearnForm5 = () => {
  let [isMarried, setIsMarried] = useState(false); //true
  return (
    <div>
      <label htmlFor="isMarried"> isMarried </label>
      <input
        id="isMarried"
        type="checkbox"
        checked={isMarried === true}
        onChange={(e) => {
          setIsMarried(e.target.checked);
        }}
      ></input>
    </div>
  );
};

export default LearnForm5;
