import React from "react";

const LearnForm1 = () => {
  return (
    <div>
      <form>
        {/* input element => general */}
        <input type="text"></input>
        <br></br>
        <input type="password"></input>
        <br></br>
        <input type="email"></input>
        <br></br>
        <input type="number"></input>
        <br></br>
        <input type="date"></input>
        <br></br>
        <input type="time"></input>
        <br></br>
        <input type="file"></input>
        {/* text area */}
        <br></br>
        <textarea></textarea>

        {/* select */}
        <br></br>

        <select>
          <option>Nepal</option>
          <option>Nepal(India)</option>
          <option>China</option>
        </select>

        {/* special input */}

        <br></br>
        <input type="radio"></input>

        <br></br>
        <input type="checkbox"></input>
      </form>
    </div>
  );
};

export default LearnForm1;
