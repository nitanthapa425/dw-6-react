import React, { useState } from "react";
import axios from "axios";

const LearnForm6 = () => {
  let [fullName, setFullName] = useState("");
  let [address, setAddress] = useState("");
  let [phoneNumber, setPhoneNumber] = useState();
  let [email, setEmail] = useState("");
  return (
    <div>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          let data = {
            fullName: fullName,
            address: address,
            phoneNumber: phoneNumber,
            email: email,
          };

          axios({
            url: "http://localhost:8000/api/v1/contacts",
            method: "POST",
            data: data,
          });
        }}
      >
        <label htmlFor="fullName">Full Name: </label>
        <input
          id="fullName"
          type="text"
          placeholder="Full Name"
          value={fullName}
          onChange={(e) => {
            setFullName(e.target.value);
          }}
        ></input>
        <br></br>

        <label htmlFor="address">Address: </label>
        <input
          id="address"
          type="text"
          placeholder="Address"
          value={address}
          onChange={(e) => {
            setAddress(e.target.value);
          }}
        ></input>
        <br></br>

        <label htmlFor="number">PhoneNumber: </label>
        <input
          id="number"
          type="number"
          placeholder="Number"
          value={phoneNumber}
          onChange={(e) => {
            setPhoneNumber(e.target.value);
          }}
        ></input>

        <br></br>

        <label htmlFor="email">Email: </label>
        <input
          id="email"
          type="email"
          placeholder="Email"
          value={email}
          onChange={(e) => {
            setEmail(e.target.value);
          }}
        ></input>
        <br></br>

        <button type="submit">Send</button>
      </form>
    </div>
  );
};

export default LearnForm6;
