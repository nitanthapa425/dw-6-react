import React, { useEffect } from "react";

const CleanUpFunction2 = () => {
  // when component gets unmount(hide)
  //nothing gets executed except clean up function

  useEffect(() => {
    let interval1 = setInterval(() => {
      console.log("i will execute for each 2s");
    }, 2000);

    return () => {
      clearInterval(interval1);
    };
  }, []);

  return <div>CleanUpFunction2</div>;
};

export default CleanUpFunction2;
