import React, { useEffect } from "react";

const CleanUpFunction = () => {
  console.log("i am component");

  // when component gets unmount(hide)
  //nothing gets executed except clean up function

  useEffect(() => {
    console.log("i am useEffect");

    return () => {
      console.log("i am clean up function");
    };
  }, []);

  return <div>CleanUpFunction</div>;
};

export default CleanUpFunction;
