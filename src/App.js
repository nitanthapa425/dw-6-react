import { useState } from "react";
import "./App.css";
import Abc from "./component/Abc";
import ClickEvent from "./component/ClickEvent";
import LearnInternalCss from "./component/LearnInternalCss";
import LearnOr from "./component/LearnOr";
import LearnTernaryOperator from "./component/LearnTernaryOperator";
import LearnToHandleArray from "./component/LearnToHandleArray";
import CleanUpFunction from "./component/LearnUseEffect/CleanUpFunction";
import LearnUseState1 from "./component/LearnUseState/LearnUseState1";
import LearnUseState10 from "./component/LearnUseState/LearnUseState10";
import LearnUseState2 from "./component/LearnUseState/LearnUseState2";
import LearnUseState3 from "./component/LearnUseState/LearnUseState3";
import LearnUseState4 from "./component/LearnUseState/LearnUseState4";
import LearnUseState6 from "./component/LearnUseState/LearnUseState6";
import LearnUseState7 from "./component/LearnUseState/LearnUseState7";
import LearnUseState8 from "./component/LearnUseState/LearnUseState8";
import LearnUseState9 from "./component/LearnUseState/LearnUseState9";
import PlayWithData from "./component/PlayWithData";
import Second from "./component/Second";
import StoreTag from "./component/StoreTag";
import CleanUpFunction2 from "./component/LearnUseEffect/CleanUpFunction2";
import LearnInfiniteLoop from "./component/LearnUseState/LearnInfiniteLoop";
import LearnForm1 from "./component/LearnForm/LearnForm1";
import LearnForm2 from "./component/LearnForm/LearnForm2";
import LearnForm3 from "./component/LearnForm/LearnForm3";
import LearnForm4 from "./component/LearnForm/LearnForm4";
import LearnForm5 from "./component/LearnForm/LearnForm5";
import LearnForm6 from "./component/LearnForm/LearnForm6";
import Routing1 from "./component/LearnRouting/Routing1";
import ReadContacts from "./component/LearnRouting/ReadContacts";

// javascript function can return only one value
//thats why the component must return only one Tag

function App() {
  let [show, setShow] = useState(true);
  return (
    <div>
      {/* <Second></Second>
      <Abc name="nitan" address="gagalphedi" age={29}></Abc>
      <div>hello</div> */}
      {/* <LearnOr></LearnOr> */}
      {/* <StoreTag></StoreTag> */}
      {/* <LearnToHandleArray></LearnToHandleArray> */}
      {/* <LearnInternalCss></LearnInternalCss> */}
      {/* <ClickEvent></ClickEvent> */}
      {/* <PlayWithData></PlayWithData> */}
      {/* <LearnTernaryOperator></LearnTernaryOperator> */}
      {/* <LearnUseState1></LearnUseState1> */}
      {/* <LearnUseState2></LearnUseState2> */}
      {/* <LearnUseState3></LearnUseState3> */}
      {/* <LearnUseState4></LearnUseState4> */}
      {/* <LearnUseState6></LearnUseState6> */}
      {/* <LearnUseState7></LearnUseState7> */}
      {/* <LearnUseState8></LearnUseState8> */}
      {/* <LearnUseState9></LearnUseState9> */}
      {/* <LearnUseState10></LearnUseState10> */}

      {/* {show ? <CleanUpFunction></CleanUpFunction> : null}

      <button
        onClick={() => {
          setShow(true);
        }}
      >
        Show
      </button>

      <button
        onClick={() => {
          setShow(false);
        }}
      >
        Hide
      </button> */}
      {/* {show ? <CleanUpFunction2></CleanUpFunction2> : null}

      <button
        onClick={() => {
          setShow(true);
        }}
      >
        Show
      </button>

      <button
        onClick={() => {
          setShow(false);
        }}
      >
        Hide
      </button> */}

      {/* <LearnInfiniteLoop></LearnInfiniteLoop> */}

      {/* <LearnForm1></LearnForm1> */}

      {/* <LearnForm2></LearnForm2> */}
      {/* <LearnForm3></LearnForm3> */}
      {/* <LearnForm4></LearnForm4> */}
      {/* <LearnForm5></LearnForm5> */}
      {/* <LearnForm6></LearnForm6> */}
      {/* <ReadContacts></ReadContacts> */}

      <Routing1></Routing1>
    </div>
  );
}

export default App;
